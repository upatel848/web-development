﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FatCalculator
{
    class Calculator
    {

        private bool isFoodLowFat = false;
        private string fatGrams = "0";
        private string totalCalories = "0";
        private string numberOfCalories = "0";
        const double FAT_LIMIT = 0.3;
        const int FAT_FACTOR = 9;

        public bool IsFoodLowFat
        {
            get {
                return isFoodLowFat;
            }
            set {
                isFoodLowFat = value;
            }
        }
        public string FatGrams
        {
            get
            {
                return fatGrams;
            }
            set
            {
                fatGrams = value;
            }
        }
        public string NumberOfCalories
        {
            get
            {
                return retrieveCaloriesCalc().ToString();
            }
            set
            {
                numberOfCalories = value;
            }
        }
        public double retrieveCaloriesCalc()
        {
            return fatGramsDb() > 0 ? (fatGramsDb() * FAT_FACTOR) : 0d;
        }

        public double retrievePerc()
        {
            return retrieveCaloriesCalc() / totalCaloriesDb();
        }
        public double totalCaloriesDb()
        {
            return Double.Parse(TotalCalories);
        }
        public double fatGramsDb()
        {
            return Double.Parse(FatGrams);
        }
        public string TotalCalories
        {
            get
            {
                return totalCalories;
            }
            set
            {
                totalCalories = value;
            }
        }
        public bool isFatLimit()
        {
            return isFoodLowFat && retrievePerc() > FAT_LIMIT;
        }
        public string validateCalc()
        {
            String returnMsg = "";
            if (totalCaloriesDb() <= 0 || fatGramsDb() <= 0)
            {
                returnMsg = "Please provide values greater than zero";
            }
            else if (totalCaloriesDb() < retrieveCaloriesCalc())
            {
                returnMsg = "Total calories should be greater than or equal to " + retrieveCaloriesCalc();
            }
            
            return returnMsg;
        }
    }
}
