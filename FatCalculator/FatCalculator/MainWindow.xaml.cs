﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace FatCalculator
{
    /*  Group 6
    Assignment 4
    Project Name: Fat Calculator
    Team Members: Elisson Cavalcante, Prathima Yarlagadda, Kalpana Bhatti and Utkarsh Patel
    */
    public partial class MainWindow : Window
    {
        private Calculator Calculator = new Calculator();

        public MainWindow()
        {
            InitializeComponent();
            DataContext = Calculator;
        }
        private void Calculate_Click(object sender, RoutedEventArgs e)
        {
            string msgError = Calculator.validateCalc();
            if (msgError.Length <= 0)
            {
                NumberOfCalories.Content = Calculator.retrieveCaloriesCalc().ToString("n2");
                PercentOfCalories.Content = Calculator.retrievePerc().ToString("p3");
                if (Calculator.IsFoodLowFat)
                {
                    String Content;
                    SolidColorBrush Color;
                    if (Calculator.isFatLimit())
                    {
                        Content = "High Fat Food";
                        Color = new SolidColorBrush(Colors.Red);
                    }
                    else
                    {
                        Content = "Low Fat Food";
                        Color = new SolidColorBrush(Colors.Green);
                    }
                    IsLowFat.Content = Content;
                    IsLowFat.Foreground = Color;
                }
            }
            else
            {
                MessageBox.Show(msgError);
            }
        }
        private void ResetValues()
        {
            NumberOfCalories.Content = "0";
            PercentOfCalories.Content = "0%";
            FatGrams.Text = "0";
            Calories.Text = "0";
            IsLowFat.Content = "";
            Calculator = new Calculator();
        }
        private void Reset_Click(object sender, RoutedEventArgs e)
        {
            ResetValues();
        }
        private void ValidateField(object sender, RoutedEventArgs e)
        {
            TextBox textBox = (TextBox)sender;
            double inputValue = 0;
            String defaultValue = "0";

            try
            {
                inputValue = Convert.ToDouble(textBox.Text);
            }
            catch
            {
                MessageBox.Show("Value informed at " + textBox.Name + " is not a valid number.", "Warning", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                textBox.Text = defaultValue;
                textBox.Dispatcher.BeginInvoke((Action)(() => { textBox.Focus(); }));
            }

            if (inputValue < 0)
            {
                MessageBox.Show("Value informed is negative " + textBox.Name + ".", "Warning", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                textBox.Text = defaultValue;
                textBox.Dispatcher.BeginInvoke((Action)(() => { textBox.Focus(); }));
            }
        }
    }
}
